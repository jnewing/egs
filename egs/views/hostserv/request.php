<?php $this->load->view('header'); ?>
	<?php $this->load->view('navigation'); ?>
	
		<!-- The content -->
		<section id="content">
		
			<h2><?php print ucfirst(strtolower($this->config->item('atheme_hostserv'))); ?> &gt; <?php _t('hs_request'); ?></h2>
		
			<form action="" method="post">
			<section>
				<label for="hostname">
					<?php _t('gen_hostname'); ?>
					<small><?php _t('hs_request_hostname_hint'); ?>.</small>
				</label>
				<div>
					<input name="hostname" id="hostname" size="35" maxlength="50" type="text" placeholder="<?php _t('gen_hostname'); ?>" class="required" />
					<br /><br />
					<input type="submit" name="submit" value="<?php _t('gen_update'); ?>" class="primary button" />
				</div>
			</section>
			</form>
					
		<div class="clear">&nbsp;</div>
		</section>
	</div>
          
<?php $this->load->view('footer'); ?>
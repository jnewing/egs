<?php $this->load->view('header'); ?>
	<?php $this->load->view('navigation'); ?>
	
		<!-- The content -->
		<section id="content">
		
			<h2><?php print ucfirst(strtolower($this->config->item('atheme_memoserv'))); ?> &gt; <?php _t('ms_send'); ?></h2>
			
			<?php if (isset($nickname) && !$nickname) : ?>
			
				<form action="" method="post">
				<section>
					<label for="nickname">
						<?php _t('gen_username'); ?>
						<small><?php _t('ms_send_nickname_hint'); ?>.</small>
					</label>
					<div>
						<input name="nickname" id="nickname" <?php print "value='{$nickname}'"; ?> size="35" maxlength="50" type="text" placeholder="<?php _t('gen_username'); ?>" class="required" />
					</div>
				</section>
				
				<section>
					<label for="thememo">
						<?php _t('gen_memo'); ?>
					</label>
					<div>
						<textarea name="thememo" id="thememo"></textarea>
						<br /><br />
						<input type="submit" name="submit" value="<?php _t('gen_send'); ?>" class="primary button" />
					</div>
				</section>				
				</form>
							
			<?php else : ?>
			
				<form action="" method="post">
				<section>
					<label for="nickname">
						<?php _t('gen_username'); ?>
						<small><?php _t('ms_send_nickname_hint'); ?>.</small>
					</label>
					<div>
						<input name="nickname" id="nickname" <?php print "placeholder='{$nickname}'"; ?> size="35" maxlength="50" type="text" class="required" />
					</div>
				</section>
				
				<section>
					<label for="thememo">
						<?php _t('gen_memo'); ?>
					</label>
					<div>
						<textarea name="thememo" id="thememo"></textarea>
						<br /><br />
						<input type="submit" name="submit" value="<?php _t('gen_send'); ?>" class="primary button" />
					</div>
				</section>				
				</form>
							
			<?php endif; ?>
			
		<div class="clear">&nbsp;</div>
		</section>
	</div>
          
<?php $this->load->view('footer'); ?>
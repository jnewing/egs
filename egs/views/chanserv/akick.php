<?php $this->load->view('header'); ?>
	<?php $this->load->view('navigation'); ?>
	
		<!-- The content -->
		<section id="content">
		
			<h2><?php print ucfirst(strtolower($this->config->item('atheme_chanserv'))); ?> &gt; <?php _t('cs_akick'); ?></h2>
			
			<div class="column left">
				<h3><?php _t('cs_akick_list'); ?></h3>
				
				<?php if (isset($response) && is_array($response)) : ?>
				<p class="hlight">
					<?php foreach ($response as $line) : ?>
						<?php print $line; ?><br />
					<?php endforeach; ?>
				</p>
				<?php endif; ?>
				
				<form action="" method="post">
				<section>
					<label for="channel">
						<?php _t('gen_channel_name'); ?>
						<small><?php _t('cs_akick_channel_hint'); ?>.</small>
					</label>
					<div>
						<input name="channel" id="channel" size="35" maxlength="50" type="text" placeholder="#<?php _t('gen_channel'); ?>" class="required" />
						<br /><br />
						<input type="submit" value="<?php _t('gen_list'); ?>" name="submit" class="primary button" />
					</div>
				
					<input type="hidden" name="list_akicks" value="1" />
				</section>
				</form>
				
			</div>
			
			<div class="column right">
				<h3><?php _t('cs_akick_manage_list'); ?></h3>
				<form action="" method="post">
				<section>
					<label for="channel">
						<?php _t('gen_channel_name'); ?>
					</label>
					<div>
						<input name="channel" id="channel" size="35" maxlength="50" type="text" placeholder="#<?php _t('gen_channel'); ?>" class="required" />
					</div>
				</section>
					
				<section>
					<label for="nick">
						<?php _t('gen_username'); ?>
					</label>
					<div>
						<input name="nick" id="nick" size="35" maxlength="50" type="text" placeholder="<?php _t('gen_username'); ?>" class="required" />
					</div>
				</section>
					
				<section>
					<label for="action">
						<?php _t('gen_action'); ?>
					</label>
					<div>
						<select name="action" id="action">
							<option value="add"><?php _t('gen_add'); ?></option>
							<option value="del"><?php _t('gen_delete'); ?></option>
						</select>	
					</div>
				</section>
					
				<section>
					<label for="reason">
						<?php _t('gen_reason') ; ?>
					</label>
					<div>
						<input name="reason" id="reason" size="35" maxlength="50" type="text" placeholder="<?php _t('gen_reason'); ?>" class="optional" />
						<br /><br />
						<input type="submit" value="<?php _t('gen_update'); ?>" class="primary button" />
					</div>
				
					<input type="hidden" name="set_akick" value="1" />
				</section>
				</form>
				
			</div>
						
			<div class="clear">&nbsp;</div>
		</section>
	</div>
          
<?php $this->load->view('footer'); ?>
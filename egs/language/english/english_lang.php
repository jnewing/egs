<?php

/**
 * Language File
 * 
 */


// GENERAL
$lang['gen_logout']                     = "Logout";
$lang['gen_login']                      = "Login";
$lang['gen_welcome_back']               = "Welcome back";
$lang['gen_welcome']                    = "Welcome";
$lang['gen_about_egs']                  = "About EGs";
$lang['gen_channel_access']             = "Channel Access";
$lang['gen_register']                   = "Register";
$lang['gen_username']                   = "Username";
$lang['gen_password']                   = "Password";
$lang['gen_dashboard']                  = "Dashboard";
$lang['gen_retype_password']            = "Re-type Password";
$lang['gen_email']                      = "Email";
$lang['gen_cancel']                     = "Cancel";
$lang['gen_channel']                    = "Channel";
$lang['gen_channel_name']               = "Channel Name";
$lang['gen_list']                       = "List";
$list['gen_action']                     = "Action";
$list['gen_add']                        = "Add";
$list['gen_delete']                     = "Delete";
$lang['gen_reason']                     = "Reason";
$lang['gen_update']                     = "Update";
$lang['gen_flags']                      = "Flags";
$lang['gen_info']                       = "Info.";
$lang['gen_clear']                      = "Clear";
$lang['gen_topic']                      = "Topic";
$lang['gen_hostname']                   = "Hostname";
$lang['gen_memo']                       = "Memo";
$lang['gen_forward']                    = "Forward";
$lang['gen_open']                       = "Open";
$lang['gen_from']                       = "From";
$lang['gen_date']                       = "Date";
$lang['gen_status']                     = "Status";
$lang['gen_actions']                    = "Actions";
$lang['gen_view']                       = "View";
$lang['gen_reply']                      = "Reply";
$lang['gen_forward']                    = "Forward";
$lang['gen_account_email']              = "Account Email";
$lang['gen_perma']                      = "Permanent";
$lang['gen_timed']                      = "Timed";
$lang['gen_type']                       = "Type";
$lang['gen_duration']                   = "Duration";
$lang['gen_enc']                        = "Encryption";
$lang['gen_access']                     = "Access";
$lang['gen_warning']                    = "Warning";
$lang['gen_module']                     = "Module";
$lang['gen_account']                    = "Account";
$lang['gen_operclass']                  = "Operclass";
$lang['gen_add']                        = "Add";
$lang['gen_delete']                     = "Delete";
$lang['gen_send']                       = "Send";

// MESSAGES
$lang['msg_success']                    = "Success";
$lang['msg_error']                      = "Error";

// NICKSERV
$lang['ns_info']                        = "Nickname Info.";
$lang['ns_password']                    = "Change Password";
$lang['ns_email']                       = "Change Email";
$lang['ns_email_desc']                  = "Change your account email using the form below.";
$lang['ns_email_email_hint']            = "New account email.";
$lang['ns_password_desc']               = "Change your account password using the form below.";
$lang['ns_password_password_hint']      = "New account password.";
$lang['ns_password_retype_hint']        = "Re-type that password.";
$lang['ns_get_info']                    = "Get Info.";
$lang['ns_info_hint']                   = "Nickname to lookup";

// CHANSERV
$lang['cs_info']                        = "Channel Info.";
$lang['cs_topic']                       = "Set Topic";
$lang['cs_kick']                        = "Kick User";
$lang['cs_ban']                         = "Ban User";
$lang['cs_akick']                       = "AKick User";
$lang['cs_akick_list']                  = "AKick List";
$lang['cs_flags']                       = "Channel Flags";
$lang['cs_ban_channel_hint']            = "Channel name you wish to ban the user from";
$lang['cs_ban_nick_hint']               = "Nickname or hostmask of the user you wish to ban";
$lang['cs_ban_unban_label']             = "Unban";
$lang['cs_ban_unban_hint']              = "Remove a ban on the above user";
$lang['cs_akick_response']              = "Channel AKick List";
$lang['cs_akick_tab_list']              = "List AKicks";
$lang['cs_akick_manage_list']           = "Manage AKick List";
$lang['cs_akick_tab_modify']            = "Modify AKicks";
$lang['cs_akick_list_channel_hint']     = "Channel whos AKick list you want to view";
$lang['cs_akick_set_channel_hint']      = "Channel whos AKick list you wish to change";
$lang['cs_akick_set_nick_hint']         = "Nickname or hostmask of the user you wish to modify";
$lang['cs_akick_set_action_hint']       = "Action you wish to preform";
$lang['cs_akick_set_reason_hint']       = "Reason you added AKick";
$lang['cs_flags_response']              = "Channel Flags List";
$lang['cs_flags_tab_list']              = "List Flags";
$lang['cs_flags_tab_change']            = "Change Flags";
$lang['cs_flags_list_channel_hint']     = "Channel whos flags you wish to list";
$lang['cs_flags_change_channel_hint']   = "Channel whos flags you wish to change";
$lang['cs_flags_change_nickname_hint']  = "Nickname or hostmask of the user you wish to change";
$lang['cs_flags_change_flags_hint']     = "Flags you wish to change";
$lang['cs_flags_toggle']				= "Toggle Flags List";
$lang['cs_info_response']               = "Channel Information";
$lang['cs_info_channel_hint']           = "Channel name you wish to view info on";
$lang['cs_kick_channel_hint']           = "Channel name you wish to kick a user from";
$lang['cs_kick_nickname_hint']          = "Nickname of the user you wish to kick";
$lang['cs_kick_reason_hint']            = "Reason you wish to kick the user";
$lang['cs_topic_channel_hint']          = "Channel name you wish to set the topic on";
$lang['cs_topic_topic_hint']            = "New channel topic you wish to set";
$lang['cs_xop']                         = "XOP";

// MEMOSERV
$lang['ms_view']                        = "View Memos";
$lang['ms_send']                        = "Send Memo";
$lang['ms_fwd']                         = "Forward Memo";
$lang['ms_delete']                      = "Delete Memo";
$lang['ms_s_reply']                     = "Reply";
$lang['ms_s_fwd']                       = "Forawrd";
$lang['ms_s_delete']                    = "Delete";
$lang['ms_del_memo_hint']               = "# of the memo you wish to delete";
$lang['ms_fwd_nickname_hint']           = "Nickname to forward the memo to";
$lang['ms_fwd_memo_hint']               = "# of the memo you wish to forward";
$lang['ms_view_from']                   = "Memo From";
$lang['ms_view_date']                   = "Date";   
$lang['ms_view_status']                 = "Status";
$lang['ms_view_memo_hint']              = "# of the memo you wish to read";
$lang['ms_send_nickname_hint']          = "Nickname to send the memo to";

// HOSTSERV
$lang['hs_offer']                       = "Offer List";
$lang['hs_request']                     = "Request";
$lang['hs_list_response']               = "Offered Hostnames";
$lang['hs_request_hostname_hint']       = "The hostname that you would like to request";
$lang['hs_take']                        = "Take";
$lang['hs_take_hint']                   = "Must be a valid offered hostname.";

// OPERSERV
$lang['os_akill']                       = "AKill";
$lang['os_soper']                       = "Soper";
$lang['os_modules']                     = "Modules";
$lang['os_rehash']                      = "Rehash";
$lang['os_passhash']                    = "Password Hash";
$lang['os_akill_nick_host']             = "Nick | Hostmask";
$lang['os_akill_added']                 = "Added By";
$lang['os_akill_expires']               = "Expires On";
$lang['os_akill_none']                  = "None found.";
$lang['os_akill_delete']                = "AKill Delete";
$lang['os_akill_add']                   = "Add AKill";
$lang['os_akill_id_hint']               = "You can also seprate multipul ID's via a , (comma)";
$lang['os_gen_hash']                    = "Generate Hash";
$lang['os_global']                      = "Global Message";
$lang['os_send_global']                 = "Send Global";
$lang['os_clear_channel']               = "Clear Channel";
$lang['os_module_warn']                 = "Be very careful while unloading modules, as some of these may be in use! Be careful! You've been warned, nuf said I'm not your mum.";
$lang['os_module_load']                 = "Load Module";
$lang['os_module_unload']               = "Unload Module";
$lang['os_rehash_check']                = "Rehash Check";
$lang['os_rehash_hint']                 = "Type &ldquo;YES&rdquo; in the box to the right (without quotes) if you really want to rehash Atheme";
$lang['os_soper_name']                  = "Soper Name";
$lang['os_soper_class']                 = "Soper Class";
$lang['os_soper_add']                   = "Add Soper";
$lang['os_soper_del']                   = "Delete Soper";

// EOF
     ______ _____
    |  ____/ ____|
    | |__ | |  __ ___
    |  __|| | |_ / __|
    | |___| |__| \__ \
    |______\_____|___/

# EGs

## About

**author** [Joseph Newing](http://twitter.com/jnewing/)

**author email** jnewing [at] gmail [dot] com

**copyright** Joseph Newing 2011 - 2012

**version** 2.3

- - -

## Legal
This file is part of EGs.

EGs is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

EGs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with EGs.  If not, see http://www.gnu.org/licenses/

- - -

## EGs Web Panel for Atheme IRC Services.

EGs is a simple web panel for Atheme IRC services. It aims to be clean, sleek and fast. With minimul installation and configuration required.

- - -

## Support

If you need support please come talk to me on /server -m irc.staticbox.net either in #atheme, or #cellsix however before doing that make sure
you read this file, at least twice.

- - -

## Installation

During this installation I'm going to assume a few things.

    1) that you know your way around a shell
    2) that you have some understanding of the mysql cli client tool
    3) That you have Atheme irc services running with both the httpd and xmlrpc modules running.


First your going to need to obtain a copy of EGs one can be found here

	https://bitbucket.org/jnewing/egs

Next up you're going to have to edit the egs/config/config.php file to reflect the settings of your web server and Atheme installation.

Cleaner URI's (**Optional**)

You may want to remove the index.php in the URI.

	http://www.yoursite.com/index.php/more/stuffhere

to something like

	http://www.yoursite.com/more/stuffhere

This is current supported on Apache, Lighttpd, Nginx and any other web server that supports some form of URI re-writing. Below I've included a few example of re-writes.

**Apache**

	<IfModule mod_rewrite.c>
		RewriteEngine On
		
		# You need to change the path to match that of your installation.
		# For example if you installed the EGs system to http://www.yoursite.com/egs/ you would 
		# change that line to read:
    	# RewriteBase /egs/
		#     
		# Alternately if your install was located on a subdomain example: http://services.epicgeeks.net 
		# we would change the RewriteBase line to read:
		# RewriteBase /
		
		RewriteBase /egs/
		
		RewriteCond %{REQUEST_URI} ^system.*
		RewriteRule ^(.*)$ /index.php?/$1 [L]
		
		RewriteCond %{REQUEST_URI} ^application.*
		RewriteRule ^(.*)$ /index.php?/$1 [L]
		
		RewriteCond %{REQUEST_FILENAME} !-f
		RewriteCond %{REQUEST_FILENAME} !-d
		RewriteRule ^(.*)$ index.php?/$1 [L]
	</IfModule>
	
	<IfModule !mod_rewrite.c>
		ErrorDocument 404 /index.php
	</IfModule>


**Lighthttpd**

	url.rewrite-once = (
 		"/(.*)\.(.*)" => "$0",
 		"/(css|files|img|js|stats)/" => "$0",
 		"^/([^.]+)$" => "/index.php/$1"
	)

**Nginx**

 	server {
    
    	listen   80;
    	
    	root /var/www/nginx-default/;
    	access_log  /var/log/nginx/localhost.access.log;
    	index index.php index.html index.htm;

    	error_page 500 502 503 504  /50x.html;
	
    	location /egs/ {
        	
        	if (-f $request_filename) {
            	expires max;
            	break;
        	}
    	
        	if (!-e $request_filename) {
            	rewrite ^/egs/(.*)$ /egs/index.php/$1 last;
        	}
    	}
    
    	location = /50x.html {
        	root /var/www/nginx-default;
    	}
	
    	location /egs/index.php {
        	fastcgi_pass 127.0.0.1:9000;
        	fastcgi_index index.php;
        	fastcgi_param SCRIPT_FILENAME /var/www/nginx-default/egs/index.php;
        	include fastcgi_params;
    	}
	}

 - - -

## Finished

That's it! You should be able to direct your browser to http://www.yoursite.com/egs/ and login using your Atheme Nickserv account nickname and password.





